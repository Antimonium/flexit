<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'flexit' );

/** MySQL database username */
define( 'DB_USER', 'user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'user' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'M^QDr!J)N@pG:^Ub*Y`IEnnr7#~~dd1$FE`w?bYUeOV6v^1gv^H4I|sC?l~0qLmr' );
define( 'SECURE_AUTH_KEY',  'A?^3If{|5V- |Mp@Auxm?6`0Vj,_`bc8Z]T,2_B$Y)-iG d+y{6godT>[Xe]o<*1' );
define( 'LOGGED_IN_KEY',    '8Qe7N3qh=7wGg8cQtYh~|D68)3#1d2x^NB;y~0NSCV<B1X9,s^GvL(kk{J;Wo,0m' );
define( 'NONCE_KEY',        'LysNQ-G_V.%A]jVu-sDH~dfHvJ J[,X=_aqW$//`oIel*Y(WZ?!eV&MyL[! 9obd' );
define( 'AUTH_SALT',        'w-0NzF/v%G~T+X*LTVlF>y}S0(Q%A .]5=LtQ-@&Y?BY,5R;6=SiQnmz4;uY81wl' );
define( 'SECURE_AUTH_SALT', 'eeA=EACG;`HjZg%s=nV{0G;Z[`Wim[]X5x ,2|D73;Je vNE3RhF_ @`_]4fT@Ss' );
define( 'LOGGED_IN_SALT',   '#t&=)i=~D7L&n%O04D@x.(!)xe2#6jO!smjFrs_PQ_rsc`H)%me`,0K.=IP}:S_h' );
define( 'NONCE_SALT',       ' 3L<stT3jHjynBEUu)}#uPw;D@QAJ6#6+Em M&->_hwC@`OLH$+*L_UyIL$<D,W0' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);