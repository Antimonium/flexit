<?php get_header(); ?>
<div class="content w-1170">
  <div class="row row_single">
    <div class="post-single">
      <div class="post-single__image">
        <img src="<?php the_field('image_item') ?>" alt="post-image"/>
        <div class="post__date">
            <?php $date = get_field('date_item');
            $date = explode(' ', $date);
            $number = substr($date[1], 0, 2);
            $month = substr(strtoupper($date[0]), 0, 3);
            ?>
          <p class="post__date-number"><?php echo $number ?></p>
          <p class="post__date-month"><?php echo $month ?></p>
        </div>
      </div>
      <div class="post-single__content">
        <h5 class="post__title"><?php the_field('title_item')?></h5>
        <p class="post-single__author"><i class="fa fa-user-o" aria-hidden="true"></i> By Admin<?php the_author(); ?></p>
        <p class="post__description"><?php the_field('description_item')?></p>
        <div class="post-single__info">
          <ul class="post-single__social">
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
          </ul>
          <p class="post-single__tags">
              <?php
              $before = '<i class="fa fa-tags" aria-hidden="true"></i><b>Tag:</b> ';
              $separator = ', ';
              the_tags( $before, $separator ); ?>
          </p>
        </div>
      </div>
    </div>
    <div class="sidebar">
      <div class="search">
        <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
          <input type="text" style="width: 240px" class="search-input" name="s" id="s" placeholder="Search..." value="<?php echo get_search_query() ?>" />
          <input type="submit"  value="" class="search-submit">
        </form>
      </div>
      <div class="sidebar__category">
        <p class="sidebar__title">CATEGORY</p>
        <ul class="sidebar__category-lists">
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Rings (256)</a></li>
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Necklaces (96)</a></li>
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Earrings (873)</a></li>
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Bracelets (622)</a></li>
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Bangles (187)</a></li>
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Beads & Charms (93)</a></li>
          <li><a href=""><i class="fa fa-angle-right" aria-hidden="true"></i>Jewellery Boxes (52)</a></li>
        </ul>
      </div>
      <div class="sidebar__posts">
        <p class="sidebar__title">RECENT POST</p>
        <div class="post-mini">
          <div class="post-mini__image"></div>
          <div class="post-mini__text">
            <a href="#">Your Blog Title goes Here</a>
            <p>15 May, 2015</p>
          </div>
        </div>
        <div class="post-mini">
          <div class="post-mini__image"></div>
          <div class="post-mini__text">
            <a href="#">Your Blog Title goes Here</a>
            <p>15 May, 2015</p>
          </div>
        </div>
        <div class="post-mini">
          <div class="post-mini__image"></div>
          <div class="post-mini__text">
            <a href="#">Your Blog Title goes Here</a>
            <p>15 May, 2015</p>
          </div>
        </div>
      </div>
      <div class="sidebar__sell">
        <p class="sidebar__title">ON SELL</p>
        <div class="post-mini post-mini_empty">
          <div class="post-mini__image_empty"></div>
          <div class="post-mini__text">
            <p class="post-mini_title">Diamond Engagement Ring</p>
            <p class="post-mini_price"><b>$352</b></p>
          </div>
        </div>
        <div class="post-mini">
          <div class="post-mini__image_empty"></div>
          <div class="post-mini__text">
            <p class="post-mini_title">Diamond Engagement Ring</p>
            <p class="post-mini_price"><b>$352</b></p>
          </div>
        </div>
        <div class="post-mini">
          <div class="post-mini__image_empty"></div>
          <div class="post-mini__text">
            <p class="post-mini_title">Diamond Engagement Ring</p>
            <p class="post-mini_price"><b>$352</b></p>
          </div>
        </div>
      </div>
      <div class="sidebar__shop-now">
        <img src="<?= get_template_directory_uri(); ?>/images/Classic_MEN’S_WATCH.png" alt="Classic_MEN’S_WATCH">
        <a class="button button_m_auto" href="<?php the_permalink(); ?>">SHOP NOW</a>
      </div>
    </div>
  </div>
</div>
<?php get_footer(); ?>