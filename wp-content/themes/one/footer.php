    <footer>
      <div class="our-brands">
        <div class="our-brands__title">
          <p class="our-brands__text">OUR BRANDS</p>
          <img src="<?= get_template_directory_uri(); ?>/images/decoration.png" alt="decoration">
        </div>
        <div class="slider">
          <div class="slider__wrapper">
            <div class="slider__carousel">
              <div class="slider__content">
                <img src="<?= get_template_directory_uri(); ?>/images/L-01.png" alt="L-01">
                <img src="<?= get_template_directory_uri(); ?>/images/L-02.png" alt="L-02">
                <img src="<?= get_template_directory_uri(); ?>/images/L-03.png" alt="L-03">
                <img src="<?= get_template_directory_uri(); ?>/images/L-04.png" alt="L-04">
                <img src="<?= get_template_directory_uri(); ?>/images/L-05.png" alt="L-05">
                <img src="<?= get_template_directory_uri(); ?>/images/L-06.png" alt="L-06">
                <img src="<?= get_template_directory_uri(); ?>/images/L-01.png" alt="L-01">
                <img src="<?= get_template_directory_uri(); ?>/images/L-02.png" alt="L-02">
                <img src="<?= get_template_directory_uri(); ?>/images/L-03.png" alt="L-03">
                <img src="<?= get_template_directory_uri(); ?>/images/L-04.png" alt="L-04">
                <img src="<?= get_template_directory_uri(); ?>/images/L-05.png" alt="L-05">
                <img src="<?= get_template_directory_uri(); ?>/images/L-06.png" alt="L-06">
                <img src="<?= get_template_directory_uri(); ?>/images/L-01.png" alt="L-01">
              </div>
            </div>
            <button class="slider__prev">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path fill="none" d="M0 0h24v24H0V0z" />
                <path d="M15.61 7.41L14.2 6l-6 6 6 6 1.41-1.41L11.03 12l4.58-4.59z" />
              </svg>
            </button>
            <button class="slider__next">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
              >
                <path fill="none" d="M0 0h24v24H0V0z" />
                <path d="M10.02 6L8.61 7.41 13.19 12l-4.58 4.59L10.02 18l6-6-6-6z" />
              </svg>
            </button>
          </div>
        </div>
      </div>
      <div class="footer__devider">
        <img class="devider__left" src="<?= get_template_directory_uri(); ?>/images/Shape_6_copy_3.png" alt="devider-left">
        <div class="footer__block-form">
          <p class="block-form__title">NEWSLETTER <br>
            <img class="block-form__decoration" src="<?= get_template_directory_uri(); ?>/images/4-layers.png" alt="form-decoration"></p>
          <form class="block-form">
            <input class="block-form__input" placeholder="Enter your Email" type="text">
            <input class="block-form__button" type="submit" value="SUBSCRIBE">
          </form>
        </div>
        <img class="devider__right" src="<?= get_template_directory_uri(); ?>/images/Shape_6_copy_2.png" alt="devider-right">
      </div>
      <div class="footer__info">
        <div class="footer__info-block w-1170">
          <ul class="column info__first-column">
            <li class="column__title">MY ACCOUNT</li>
            <li><a href="#">My Orders</a></li>
            <li><a href="#">My Credit Ships</a></li>
            <li><a href="#">My Adresses</a></li>
            <li><a href="#">My Presonal Info</a></li>
          </ul>
          <ul class="column info__first-column">
            <li class="column__title">ORDERS</li>
            <li><a href="#">Payment Options</a></li>
            <li><a href="#">Shipping</a></li>
            <li><a href="#">Returns</a></li>
            <li><a href="#">Delivery</a></li>
          </ul>
          <ul class="column info__first-column">
            <li class="column__title">INFORMATION</li>
            <li><a href="#">About us</a></li>
            <li><a href="#">Delivery information</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Custom Service</a></li>
          </ul>
          <ul class="column info__first-column">
            <li class="column__title">CONTACT US</li>
            <li class="column__contact column__contact_adress"><a href="#"><i class="fa fa-map-marker h-20 fa_pr" aria-hidden="true"></i><span class="column__contact-adress">68 Dohava Stress, Lorem isput Spusts New York - United State</span></a></li>
            <li class="column__contact"><a href="#"><i class="fa fa-envelope h-20 fa_pr" aria-hidden="true"></i><span>info@youremail.com</span></a></li>
            <li class="column__contact"><a href="#"><i class="fa fa-phone h-20 fa_pr" aria-hidden="true"></i><span>+1 (00) 42 868 666 888</span></a></li>
          </ul>
        </div>
        <div class="footer__social w-1170">
          <ul>
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
          </ul>
          <img src="<?= get_template_directory_uri(); ?>/images/cards.png" alt="credit-cards">
          <a class="footer__anchor" href="#"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="footer__copy">
        <p class="copy__right">Copyright © 2016, <span>Theme_Express</span> All Rights Reseved.</p>
      </div>
    </footer>
    <?php the_my_js_css() ?>
    <?php wp_footer(); ?>
  </body>
</html>