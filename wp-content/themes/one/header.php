<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/font-awesome-4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
  <title>Document</title>
  <?php wp_head(); ?>
</head>
<body>
<header class="header">
    <div class="header__top">
      <div class="header__top-block w-1170">
        <div class="mobile-menu__button">
          <button class="menu__button"><i class="fa fa-bars" aria-hidden="true"></i></button>
        </div>
        <div class="mobile-menu__list">
            <?php wp_nav_menu(array('menu' => 'navigation_top')); ?>
        </div>

        <a href="#" class="header__email"><i class="fa fa-envelope grey h-13" aria-hidden="true"></i><span>info@youremail.com</span> </a>
        <a href="#" class="header__phone"><i class="fa fa-phone grey h-13" aria-hidden="true"></i><span>+(56) 123 456 546</span> </a>
        <a href="#" class="header__account">My Account</a>
      </div>
    </div>
    <div class="header__middle w-1170">
      <img src="<?= get_template_directory_uri(); ?>/images/Logo_.png" alt="logo">
      <?php get_search_form(); ?>
    </div>
    <div class="header__navigation">
      <div class="header__navigation-block w-1170">
        <nav>
            <?php wp_nav_menu(array('menu' => 'navigation_top')); ?>
        </nav>


        <div class="header__social">
          <ul>
            <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
            <li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="header__bottom">
      <img src="<?= get_template_directory_uri(); ?>/images/Blog_LATEST_NEWS.png" alt="latest-news">
      <p>Home &nbsp;<i class="fa fa-angle-right" aria-hidden="true"></i>&nbsp; <a href="#">News Details</a></p>
    </div>

</header>
