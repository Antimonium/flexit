<div class="search">
    <form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
        <input type="text" class="search-input" name="s" id="s" placeholder="Inter Your Keyword..." value="<?php echo get_search_query() ?>" />
        <input type="submit" value="" class="search-submit">
    </form>
</div>