<?php
function enqueue_styles() {
    wp_enqueue_style( 'one-style', get_stylesheet_uri());
    wp_enqueue_style( 'reset', get_stylesheet_directory_uri() . '/style/reset.css' );
    wp_enqueue_style( 'header', get_stylesheet_directory_uri() . '/style/header.css' );
    wp_enqueue_style( 'footer', get_stylesheet_directory_uri() . '/style/footer.css' );
    wp_enqueue_style( 'content', get_stylesheet_directory_uri() . '/style/content.css' );
    wp_enqueue_style( 'sidebar', get_stylesheet_directory_uri() . '/style/sidebar.css' );
    wp_enqueue_style( 'responsive-css', get_stylesheet_directory_uri() . '/style/responsive.css' );
    wp_enqueue_style( 'font-style');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');


function remove_css_from_wp_head() {
    wp_dequeue_style( 'colors' );
    wp_dequeue_style( 'acf-dark' );
    wp_dequeue_style( 'acf-global' );
    wp_dequeue_style( 'acf-datepicker' );
    wp_dequeue_style( 'akismet.css' );
    wp_dequeue_style( 'twentytwenty-style' );
    wp_dequeue_style( 'twentytwenty-print-style' );
    wp_dequeue_style( 'wp_admin_bar_header' );
}

add_action( 'wp_enqueue_scripts', 'remove_css_from_wp_head', 9999 );
remove_action( 'wp_head', 'wp_admin_bar_header' );

function my_scripts_method() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js');
    wp_enqueue_script( 'jquery' );

    wp_enqueue_script( 'slider', get_template_directory_uri() . '/js/slider.js');
    wp_enqueue_script( 'mobile_menu', get_template_directory_uri() . '/js/mobile_menu.js');
}

add_action( 'wp_enqueue_scripts', 'my_scripts_method', 11 );

remove_action( 'wp_head',   'wp_print_head_scripts',    9 );
remove_action( 'wp_footer', 'wp_print_footer_scripts', 20 );

// пере-подключаем
add_action( 'my_print_stsc', 'wp_print_head_scripts' );
add_action( 'my_print_stsc', 'wp_print_footer_scripts' );

function the_my_js_css(){

    do_action('my_print_stsc');
}

/*
 * Функция создает дубликат поста в виде черновика и редиректит на его страницу редактирования
 */
function true_duplicate_post_as_draft(){
    global $wpdb;
    if (! ( isset( $_GET['post']) || isset( $_POST['post'])  || ( isset($_REQUEST['action']) && 'true_duplicate_post_as_draft' == $_REQUEST['action'] ) ) ) {
        wp_die('Нечего дублировать!');
    }

    /*
       * получаем ID оригинального поста
       */
    $post_id = (isset($_GET['post']) ? $_GET['post'] : $_POST['post']);
    /*
       * а затем и все его данные
       */
    $post = get_post( $post_id );

    /*
       * если вы не хотите, чтобы текущий автор был автором нового поста
       * тогда замените следующие две строчки на: $new_post_author = $post->post_author;
       * при замене этих строк автор будет копироваться из оригинального поста
       */
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    /*
       * если пост существует, создаем его дубликат
       */
    if (isset( $post ) && $post != null) {

        /*
             * массив данных нового поста
             */
        $args = array(
            'comment_status' => $post->comment_status,
            'ping_status'    => $post->ping_status,
            'post_author'    => $new_post_author,
            'post_content'   => $post->post_content,
            'post_excerpt'   => $post->post_excerpt,
            'post_name'      => $post->post_name,
            'post_parent'    => $post->post_parent,
            'post_password'  => $post->post_password,
            'post_status'    => 'draft', // черновик, если хотите сразу публиковать - замените на publish
            'post_title'     => $post->post_title,
            'post_type'      => $post->post_type,
            'to_ping'        => $post->to_ping,
            'menu_order'     => $post->menu_order
        );

        /*
             * создаем пост при помощи функции wp_insert_post()
             */
        $new_post_id = wp_insert_post( $args );

        /*
             * присваиваем новому посту все элементы таксономий (рубрики, метки и т.д.) старого
             */
        $taxonomies = get_object_taxonomies($post->post_type); // возвращает массив названий таксономий, используемых для указанного типа поста, например array("category", "post_tag");
        foreach ($taxonomies as $taxonomy) {
            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
            wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
        }

        /*
             * дублируем все произвольные поля
             */
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
        if (count($post_meta_infos)!=0) {
            $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
            $sql_query.= implode(" UNION ALL ", $sql_query_sel);
            $wpdb->query($sql_query);
        }


        /*
             * и наконец, перенаправляем пользователя на страницу редактирования нового поста
             */
        wp_redirect( admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
        exit;
    } else {
        wp_die('Ошибка создания поста, не могу найти оригинальный пост с ID=: ' . $post_id);
    }
}
add_action( 'admin_action_true_duplicate_post_as_draft', 'true_duplicate_post_as_draft' );

/*
 * Добавляем ссылку дублирования поста для post_row_actions
 */
function true_duplicate_post_link( $actions, $post ) {
    if (current_user_can('edit_posts')) {
        $actions['duplicate'] = '<a href="admin.php?action=true_duplicate_post_as_draft&post=' . $post->ID . '" title="Дублировать этот пост" rel="permalink">Дублировать</a>';
    }
    return $actions;
}

add_filter( 'post_row_actions', 'true_duplicate_post_link', 10, 2 );