<?php get_header(); ?>

<?php

$current_page = !empty($_GET['section']) ? $_GET['section'] : 1;
$query = new WP_Query(array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'paged' => $current_page,
        'posts_per_page' => 9)
);

$paged_page_nav = paginate_links(array(
    'base' => site_url() . '%_%',
    'format' => '?section=%#%',
    'total' => $query->max_num_pages,
    'current' => $current_page,
    'prev_text' => __('<i class="fa fa-angle-left" aria-hidden="true"></i>'),
    'next_text' => __('<i class="fa fa-angle-right" aria-hidden="true"></i>'),
    'mid_size' => 3,
    'end_size' => 2,
    'echo' => false,
));
$paged_page_nav = str_replace('<a', '<li><a', $paged_page_nav);
$paged_page_nav = str_replace('/a>', '/a></li>', $paged_page_nav);
$paged_page_nav = str_replace('<span', '<li><a', $paged_page_nav);
$paged_page_nav = str_replace('/span>', '/a></li>', $paged_page_nav);
$paged_page_nav = str_replace('page-numbers current', 'pstr-active', $paged_page_nav);

?>

<div class="content w-1170">
  <div class="row">
    <?php if ( $query->have_posts() ) : ?>
      <?php while ( $query->have_posts() ) : $query->the_post(); ?>
      <div class="post">
        <div class="post__image">
          <img src="<?php the_field('image_item') ?>" alt="post-image"/>
          <div class="post__date">
              <?php $date = get_field('date_item');
              $date = explode(' ', $date);
              $number = substr($date[1], 0, 2);
              $month = substr(strtoupper($date[0]), 0, 3);
              ?>
            <p class="post__date-number"><?php echo $number ?></p>
            <p class="post__date-month"><?php echo $month ?></p>
          </div>
        </div>
        <div class="post__content">
          <h5 class="post__title"><?php the_field('title_item')?></h5>
          <p class="post__description"><?php the_field('description_item')?></p>

          <div class="post__info">
            <div class="post__info-block">
              <a class="button" href="<?php the_permalink(); ?>">MORE</a>
              <p class="post__comments"><img src="<?= get_template_directory_uri(); ?>/images/comment.png" alt="comment">15 <span>Comments</span></p>
              <p class="post__likes"><img src="<?= get_template_directory_uri(); ?>/images/like.png" alt="like">51 <span>Likes</span></p>
            </div>
          </div>
        </div>
      </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    <?php else : ?>
      <p><?php _e( 'Извините, нет записей, соответствуюших Вашему запросу.' ); ?></p>
    <?php endif; ?>
  </div>

  <div class="block_pagination">
    <div class="pstrnav">
        <?php  echo '<ul>' . $paged_page_nav . '</ul>'; ?>
    </div>
  </div>

</div>
<?php get_footer(); ?>